#!/bin/bash
#set +e  # continue if any command fails

# remove old unit test xml data
rm -f *.xml

echo "----------------- AuxIO_TB"
ghdl -r AuxIO_TB       --ieee-asserts=disable --stop-time=10ms 
echo "----------------- LEDControl_TB"
ghdl -r LEDControl_TB  --ieee-asserts=disable --stop-time=1ms  
echo "----------------- ABSCamera_TB"
ghdl -r ABSCamera_TB   --ieee-asserts=disable --stop-time=1ms  
echo "----------------- Done"


