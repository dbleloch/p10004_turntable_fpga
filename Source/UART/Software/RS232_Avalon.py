"""Class for doing AvalonRead/Writes through RS232 to a VHDL Avlon Master"""

import serial
import time
import _winreg #required for the windows registery look up to found com port

"""Look in the registry fo rthe first com port"""
def FindComPortInRegistry():
    key = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, r'Hardware\DeviceMap\SerialComm', 0, _winreg.KEY_ALL_ACCESS)
    for i in range(20): # if a USB device is unplugged then replugged it is enumerated with an incremented value
        regstring = "\Device\VCP%s" %str(i)
        try:
            com_port = _winreg.QueryValueEx(key, regstring)
        except:
            pass
    return com_port[0]
    
def ValueToBinarySerial(value):
    hex_value = "%0.8X" %value
    binary_serial = chr(int(hex_value[0:2],16)) + chr(int(hex_value[2:4],16)) + chr(int(hex_value[4:6],16)) + chr(int(hex_value[6:8],16))
    # print binary_serial
    return binary_serial
    
def BinarySerialToValue(binary_serial):
    ReadDataString = []
    for letter in xrange(len(binary_serial)):
        number = ord(binary_serial[letter]) # ord converts a binary 'character' byte to an 8-bit integer (representing 2 ASCII characters)
        ReadDataString.append(str(hex(number/16))[2]) # convert top nibble to a hex number as an ASCII character
        ReadDataString.append(str(hex(number%16))[2]) # convert lower nibble to a hex number as an ASCII character
    return ''.join(ReadDataString) # join all the hex characters together in a single string and return it

""" Class to talk via RS232 Comm ports to FPGA Avalon Master"""
class RS232_Avalon(object):
    """Constructor if no com port number is specified than an attempt will be made to find the 1st in the windows registry."""
    def __init__ (self, com_port_number=None,baudrate=3e6,timeout=1):
        if com_port_number==None:
            com_port_number=FindComPortInRegistry()
            print "Found connection on Com Port ", com_port_number
        ###############################
        #Setup Com port 
        ###############################            
        self.ser = serial.Serial(com_port_number)
        self.ser.timeout = timeout
        self.ser.baudrate = baudrate

    """Destructor deletes connection to com port"""
    def __del__(self):
        self.ser.close()

    """AvalonWrite( address , data) """ 
    def AvalonWrite(self,address,data):    
        code = "%c%0.8X%0.8X" %('W',address,data)
        self.ser.write(code)
        
    def BinaryWrite(self,address,data):
        code = chr(0x54) + ValueToBinarySerial(address) + ValueToBinarySerial(data)
        self.ser.write(code)
        return
        
    def multi_write(self,command,address,datalist):
        code = chr(command) + ValueToBinarySerial(address) + ValueToBinarySerial(len(datalist))
        self.ser.write(code)
        for entry in datalist:
            code = ValueToBinarySerial(entry)
            self.ser.write(code)
            
    def multi_read(self, command, address, count):
        code = chr(command) + ValueToBinarySerial(address) + ValueToBinarySerial(count)
        self.ser.write(code)
        ReadDataList = []
        for i in range(count):
            time.sleep(0.1)
            data = self.ser.read(4)
            if len(data)==0:
                raise ValueError, "No data returned"   
            ReadDataList.append(BinarySerialToValue(data))
        return ReadDataList
            
        
    def BinaryRead(self, address):
        code = chr(0x4F) + ValueToBinarySerial(address)
        self.ser.write(code)
        time.sleep(0.1)
        data = self.ser.read(4)
        if len(data)==0:
            raise ValueError, "No data returned"   
        ReadDataString = BinarySerialToValue(data)
        return ReadDataString
    
    """AvalonWrite( address) """ 
    def AvalonRead(self,address):
        code = "%c%0.8X" %('R',address)
        self.ser.write(code)
        time.sleep(0.1)
        #try:
        data = self.ser.read(8)
        if len(data)==0:
            raise ValueError, "No data returned"    
        ReadDataString = "%0.8x" %(int(data,16))
        return(ReadDataString)
     
###############################
# Simple Test case
###############################
if __name__ == "__main__":
    
    rs232_avalon = RS232_Avalon()
    rs232_avalon.AvalonWrite(1234,5678)
    print "0x%08x" %(rs232_avalon.AvalonRead(0x4))
    del(rs232_avalon)
    
    
    
    