-------------------------------------------------------------
-- Xyratex CONFIDENTIAL 
-- (C) Copyright Xyratex 2012 All rights reserved
-------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.ALL;

use work.rng_lib.all;

library C;
use C.stdio_h.ALL;



entity uart_tb is 
end entity uart_tb;

architecture uart_tb_arch of uart_tb is

    component uart_tx is
	Port(	
        --Misc
        Clock                   : in  std_logic;
        Reset                   : in  std_logic;
        -- Avalon
        AvalonStream_data       : in  std_logic_vector(7 downto 0);
        AvalonStream_valid      : in  std_logic;
        AvalonStream_ready      : out std_logic;
        -- Uart control
        
        UartClockEnable         : in  std_logic;

        -- Uart Interface
        TX                      : out std_logic	
	);
    end component uart_tx;

    component uart_rx is
	Port(	
        --Misc
        Clock                   : in  std_logic;
        Reset                   : in  std_logic;
        -- Avalon
        AvalonStream_data       : out  std_logic_vector(7 downto 0);
        AvalonStream_valid      : out  std_logic;

        -- Uart control
        UartClockCount          : in  std_logic_vector(15 downto 0);

        -- Uart Interface
        rx                      : in std_logic
	);
    end component uart_rx;



    signal Clock                   :std_logic;
    signal Reset                   :std_logic;
    signal TX_AvalonStream_data    :std_logic_vector(7 downto 0);
    signal TX_AvalonStream_valid   :std_logic;
    signal TX_AvalonStream_ready   :std_logic;
    signal RX_AvalonStream_data    :std_logic_vector(7 downto 0);
    signal RX_AvalonStream_valid   :std_logic;
    signal UartClockEnable         :std_logic;
    signal TX                      :std_logic;
    signal RX                      :std_logic;

    type dataset_t is array (0 to 3) of std_logic_vector(7 downto 0);
    
    constant dataset_out : dataset_t := (x"AA", x"00", x"55", x"FF");


begin


    U_uart_tx: uart_tx 
	port map(	
        --Misc
        Clock                  => Clock             ,
        Reset                  => Reset             ,
        -- Avalon              
        AvalonStream_data      => TX_AvalonStream_data ,
        AvalonStream_valid     => TX_AvalonStream_valid,
        AvalonStream_ready     => TX_AvalonStream_ready,
        -- Uart control        
        UartClockEnable        => UartClockEnable   ,

        -- Uart Interface
        TX                     => TX                     
    );
    
    U_uart_rx: uart_rx
    port map(
        --Misc
        Clock                   => Clock             ,
        Reset                   => Reset             ,
        -- Avalon
        AvalonStream_data       => RX_AvalonStream_data ,
        AvalonStream_valid      => RX_AvalonStream_valid,

        -- Uart control
        UartClockCount          => x"0015",

        -- Uart Interface
        rx                      => rx
    );
    
    rx <= tx; 
    
    
    process
    begin
        Clock <= '0';
        wait for 5 ns;
        Clock <= '1';
        wait for 5 ns;
    end process;
    
    Reset <= '1', '0' after 5 ns;
    
    process
    begin
        UartClockEnable <= '0';
        for i in 0 to 20 loop
            wait until rising_edge(Clock);
        end loop;
        UartClockEnable <= '1';
        wait until rising_edge(Clock);
    end process;
    
    
    main_P:  process
        variable r_uni : rand_var; --don't initialise here as Modelsim crahses if upper and lower limit the same
        variable delay : integer;

    begin
        r_uni := init_uniform(0,0,0,real(3000),real(4000)); 
        loop
            for i in 0 to 3 loop
                r_uni := rand(r_uni);
                
                delay   := integer(r_uni.rnd);
                printf("Delay = %d ns \n", delay);
    
                TX_AvalonStream_data  <=x"11"; 
                TX_AvalonStream_valid <='0';
                wait for delay * 1 ns;
                wait until rising_edge(Clock);
                TX_AvalonStream_data  <=dataset_out(i);
                TX_AvalonStream_valid <='1';
                
                wait until rising_edge(Clock) and TX_AvalonStream_ready='1';
                
            end loop;
        end loop;
        
        wait;
    end process main_P;

    process (Clock)
    begin
        if rising_edge(Clock) then
            if RX_AvalonStream_valid='1' then
                printf("Received 0x%x \n", RX_AvalonStream_data);
            end if;
        end if;
    end process;
    


end architecture uart_tb_arch;

