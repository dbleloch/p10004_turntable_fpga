-------------------------------------------------------------
-- Xyratex CONFIDENTIAL 
-- (C) Copyright Xyratex 2012 All rights reserved
-------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.ALL;

entity uart_tx_tb is 
end entity uart_tx_tb;

architecture uart_tx_tb_arch of uart_tx_tb is



    component uart_tx is
	Port(	
        --Misc
        Clock                   : in  std_logic;
        Reset                   : in  std_logic;
        -- Avalon
        AvalonStream_data       : in  std_logic_vector(7 downto 0);
        AvalonStream_valid      : in  std_logic;
        AvalonStream_ready      : out std_logic;
        -- Uart control
        
        UartClockEnable         : in  std_logic;

        -- Uart Interface
        TX                      : out std_logic	
	);
    end component uart_tx;

    signal Clock                   :std_logic;
    signal Reset                   :std_logic;
    signal AvalonStream_data       :std_logic_vector(7 downto 0);
    signal AvalonStream_valid      :std_logic;
    signal AvalonStream_ready      :std_logic;
    signal UartClockEnable         :std_logic;
    signal TX                      :std_logic;


begin


    U_uart_tx: uart_tx 
	port map(	
        --Misc
        Clock                  => Clock             ,
        Reset                  => Reset             ,
        -- Avalon              
        AvalonStream_data      => AvalonStream_data ,
        AvalonStream_valid     => AvalonStream_valid,
        AvalonStream_ready     => AvalonStream_ready,
        -- Uart control        
        UartClockEnable        => UartClockEnable   ,

        -- Uart Interface
        TX                     => TX                     
    );
    
    
    process
    begin
        Clock <= '0';
        wait for 5 ns;
        Clock <= '1';
        wait for 5 ns;
    end process;
    
    Reset <= '1', '0' after 5 ns;
    
    process
    begin
        UartClockEnable <= '0';
        for i in 0 to 20 loop
            wait until rising_edge(Clock);
        end loop;
        UartClockEnable <= '1';
        wait until rising_edge(Clock);
    end process;
    
    
    main_P:  process
    begin
        AvalonStream_data <= x"AA";
        AvalonStream_valid <='0';
        wait for 50 ns;
        wait until rising_edge(Clock);
        AvalonStream_valid <='1';
        wait until rising_edge(Clock);
        AvalonStream_valid <='0';
        wait;
    end process main_P;
    


end architecture uart_tx_tb_arch;

