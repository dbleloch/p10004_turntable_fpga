-------------------------------------------------------------
-- Xyratex CONFIDENTIAL 
-- (C) Copyright Xyratex 2012 All rights reserved
-------------------------------------------------------------
library ieee ;
use ieee.std_logic_1164.all ;

library work;
use work.UART_Testbench_Package.ALL;

entity TB_UART_to_Avalon_MM_Master is
    generic(
      clk_period : time := 10 ns; -- 100 MHz
      baud_delay : time := 50 ns
    );
end entity TB_UART_to_Avalon_MM_Master;

architecture TB_UART_to_Avalon_MM_Master_arch of TB_UART_to_Avalon_MM_Master is

    component UART_to_Avalon_MM_Master is
        port(	
        --------- avalon signals -------------
            MasterPortAvalonClock           : in std_logic;
            MasterPortAvalonReset           : in std_logic;

            MasterPortAvalonAddress         : out std_logic_vector(31 downto 0); 
            MasterPortAvalonWrite           : out std_logic;                     
            MasterPortAvalonWriteData       : out std_logic_vector(31 downto 0); 
            MasterPortAvalonRead            : out std_logic;
            MasterPortAvalonReadData        : in std_logic_vector(31 downto 0);
            MasterPortAvalonChipSelect      : out std_logic;
            MasterPortAvalonWaitRequest     : in std_logic;
            
        ----------------------------------------------------
            TX     : out std_logic;
            RX	   : in std_logic
            );
    end component;

    signal clock : std_logic := '0';
    signal reset : std_logic := '0';
    signal Rx    : std_logic := '1';
    
    
begin
    
    clock <= not clock after clk_period /2;
    Reset <= '1','0' after 2*clk_period;
    
    U_UART_to_Avalon_MM_Master : UART_to_Avalon_MM_Master
        port map(	
            MasterPortAvalonClock           => clock,
            MasterPortAvalonReset           => reset,

            MasterPortAvalonAddress         => open,
            MasterPortAvalonWrite           => open,
            MasterPortAvalonWriteData       => open,
            MasterPortAvalonRead            => open,
            MasterPortAvalonReadData        => x"12345678",
            MasterPortAvalonChipSelect      => open,
            MasterPortAvalonWaitRequest     => '0',
            
            TX     => open,
            RX	   => Rx
            );
    
    UART_Write_P: process
    begin
        wait for 1 us;
        UARTWrite(X"54",baud_delay,clock,Rx); -- binary write
        UARTWrite(X"00",baud_delay,clock,Rx); -- address 00001000
        UARTWrite(X"00",baud_delay,clock,Rx);
        UARTWrite(X"10",baud_delay,clock,Rx);
        UARTWrite(X"00",baud_delay,clock,Rx);
        UARTWrite(X"80",baud_delay,clock,Rx); -- data 80001122
        UARTWrite(X"00",baud_delay,clock,Rx); 
        UARTWrite(X"11",baud_delay,clock,Rx); 
        UARTWrite(X"22",baud_delay,clock,Rx); 
        wait for 1 us;
        
        UARTWrite(X"4F",baud_delay,clock,Rx); -- binary read
        UARTWrite(X"A2",baud_delay,clock,Rx); -- address A2004020
        UARTWrite(X"00",baud_delay,clock,Rx);
        UARTWrite(X"40",baud_delay,clock,Rx);
        UARTWrite(X"20",baud_delay,clock,Rx);
        wait for 2 us;
        
        UARTWrite(X"53",baud_delay,clock,Rx); -- static read
        UARTWrite(X"00",baud_delay,clock,Rx); -- address 00004000
        UARTWrite(X"00",baud_delay,clock,Rx);
        UARTWrite(X"40",baud_delay,clock,Rx);
        UARTWrite(X"00",baud_delay,clock,Rx);
        UARTWrite(X"00",baud_delay,clock,Rx); -- number of reads 4
        UARTWrite(X"00",baud_delay,clock,Rx);
        UARTWrite(X"00",baud_delay,clock,Rx);
        UARTWrite(X"04",baud_delay,clock,Rx);
        wait for 10 us;
        
        UARTWrite(X"49",baud_delay,clock,Rx); -- incrementing read
        UARTWrite(X"00",baud_delay,clock,Rx); -- address 00008000
        UARTWrite(X"00",baud_delay,clock,Rx);
        UARTWrite(X"80",baud_delay,clock,Rx);
        UARTWrite(X"00",baud_delay,clock,Rx);
        UARTWrite(X"00",baud_delay,clock,Rx); -- number of reads 6
        UARTWrite(X"00",baud_delay,clock,Rx);
        UARTWrite(X"00",baud_delay,clock,Rx);
        UARTWrite(X"06",baud_delay,clock,Rx);
        wait for 19 us;
        
        UARTWrite(X"50",baud_delay,clock,Rx); -- static write
        UARTWrite(X"00",baud_delay,clock,Rx); -- address 00010000
        UARTWrite(X"01",baud_delay,clock,Rx);
        UARTWrite(X"00",baud_delay,clock,Rx);
        UARTWrite(X"00",baud_delay,clock,Rx);
        UARTWrite(X"00",baud_delay,clock,Rx); -- number of writes 3
        UARTWrite(X"00",baud_delay,clock,Rx);
        UARTWrite(X"00",baud_delay,clock,Rx);
        UARTWrite(X"03",baud_delay,clock,Rx);
        UARTWrite(X"01",baud_delay,clock,Rx); -- data 1 01010101
        UARTWrite(X"01",baud_delay,clock,Rx); 
        UARTWrite(X"01",baud_delay,clock,Rx); 
        UARTWrite(X"01",baud_delay,clock,Rx); 
        UARTWrite(X"02",baud_delay,clock,Rx); -- data 2 02020202
        UARTWrite(X"02",baud_delay,clock,Rx); 
        UARTWrite(X"02",baud_delay,clock,Rx); 
        UARTWrite(X"02",baud_delay,clock,Rx); 
        UARTWrite(X"03",baud_delay,clock,Rx); -- data 3 03030303
        UARTWrite(X"03",baud_delay,clock,Rx); 
        UARTWrite(X"03",baud_delay,clock,Rx); 
        UARTWrite(X"03",baud_delay,clock,Rx); 
        wait for 2 us;
        
        UARTWrite(X"4D",baud_delay,clock,Rx); -- incrementing write
        UARTWrite(X"00",baud_delay,clock,Rx); -- address 00040000
        UARTWrite(X"04",baud_delay,clock,Rx);
        UARTWrite(X"00",baud_delay,clock,Rx);
        UARTWrite(X"00",baud_delay,clock,Rx);
        UARTWrite(X"00",baud_delay,clock,Rx); -- number of writes 5
        UARTWrite(X"00",baud_delay,clock,Rx);
        UARTWrite(X"00",baud_delay,clock,Rx);
        UARTWrite(X"05",baud_delay,clock,Rx);
        UARTWrite(X"11",baud_delay,clock,Rx); -- data 1 1111AAAA
        UARTWrite(X"11",baud_delay,clock,Rx); 
        UARTWrite(X"AA",baud_delay,clock,Rx); 
        UARTWrite(X"AA",baud_delay,clock,Rx); 
        UARTWrite(X"22",baud_delay,clock,Rx); -- data 2 2222BBBB
        UARTWrite(X"22",baud_delay,clock,Rx); 
        UARTWrite(X"BB",baud_delay,clock,Rx); 
        UARTWrite(X"BB",baud_delay,clock,Rx); 
        UARTWrite(X"33",baud_delay,clock,Rx); -- data 3 3333CCCC
        UARTWrite(X"33",baud_delay,clock,Rx); 
        UARTWrite(X"CC",baud_delay,clock,Rx); 
        UARTWrite(X"CC",baud_delay,clock,Rx); 
        UARTWrite(X"44",baud_delay,clock,Rx); -- data 4 4444DDDD
        UARTWrite(X"44",baud_delay,clock,Rx); 
        UARTWrite(X"DD",baud_delay,clock,Rx); 
        UARTWrite(X"DD",baud_delay,clock,Rx); 
        UARTWrite(X"55",baud_delay,clock,Rx); -- data 5 5555EEEE
        UARTWrite(X"55",baud_delay,clock,Rx); 
        UARTWrite(X"EE",baud_delay,clock,Rx); 
        UARTWrite(X"EE",baud_delay,clock,Rx); 
        wait for 2 us;
        
        -- UARTWriteS("W000010040000000A",baud_delay,clock,RX);
        -- wait for 2 us;
        -- UARTWriteS("R00002004",baud_delay,clock,RX);

        wait for 10 us;
        report "End of test" severity failure;
        
        
    end process UART_Write_P;
    

end architecture TB_UART_to_Avalon_MM_Master_arch;


