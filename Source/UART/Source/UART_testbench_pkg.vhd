-------------------------------------------------------------
-- Xyratex CONFIDENTIAL 
-- (C) Copyright Xyratex 2010 All rights reserved
-------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.ALL;
USE ieee.std_logic_arith.all;

package UART_Testbench_Package is

    type charstring is array(integer range <>) of character;
    
    procedure UARTWriteS( constant StringToWrite : in charstring;
                         constant baud_delay : in time;
                         signal WBClock : in std_logic;
                         signal sRX : out std_logic
                         );
                         
    -- procedure UARTWriteB( constant BinaryDataToWrite : in charstring;
                         -- constant baud_delay : in time;
                         -- signal WBClock : in std_logic;
                         -- signal sRX : out std_logic
                         -- );

	procedure UARTWrite( constant DataToWrite : in std_logic_vector;
                         constant baud_delay : in time;
                         signal WBClock : in std_logic;
                         signal sRX : out std_logic
                         );
                         
    procedure UARTWrite( constant LetterToWrite : in character;
                         constant baud_delay : in time;
                         signal WBClock : in std_logic;
                         signal sRX : out std_logic
                         );
    
end package UART_Testbench_Package;

package body UART_Testbench_Package is

    
    procedure UARTWriteS( constant StringToWrite : in charstring;
                         constant baud_delay : in time;
                         signal WBClock : in std_logic;
                         signal sRX : out std_logic
                         )
        is begin
            for i in StringToWrite'RANGE loop
                UARTWrite(StringToWrite(i),baud_delay,WBClock,sRX);
            end loop;
    end procedure UARTWriteS;
    
    -- procedure UARTWriteB( constant BinaryDataToWrite : in charstring;
                         -- constant baud_delay : in time;
                         -- signal WBClock : in std_logic;
                         -- signal sRX : out std_logic
                         -- )
        -- is begin
            -- for i in BinaryDataToWrite'HIGH/2 downto 0 loop
                -- report 
                -- UARTWrite(BinaryDataToWrite(BinaryDataToWrite'HIGH-i) & BinaryDataToWrite(BinaryDataToWrite'HIGH-2*i),baud_delay,WBClock,sRX);
            -- end loop;
    -- end procedure UARTWriteB;
            
    
    procedure UARTWrite( constant LetterToWrite : in character;
                         constant baud_delay : in time;
                         signal WBClock : in std_logic;
                         signal sRX : out std_logic
                         )
        is begin
            UARTWrite(('0' & CONV_STD_LOGIC_VECTOR(character'pos(LetterToWrite),7)),baud_delay,WBClock,sRX);
    end procedure UARTWrite;
        
    
    procedure UARTWrite( constant DataToWrite : in std_logic_vector;
                         constant baud_delay : in time;
                         signal WBClock : in std_logic;
                         signal sRX : out std_logic
                         ) 
        is begin
            wait until rising_edge(WBClock);
            sRX <= '0'; -- START
            wait for baud_delay;
            
            sRX <= DataToWrite(7);
            wait for baud_delay;
            sRX <= DataToWrite(6);
            wait for baud_delay;
            sRX <= DataToWrite(5);
            wait for baud_delay;
            sRX <= DataToWrite(4);
            wait for baud_delay;
            sRX <= DataToWrite(3);
            wait for baud_delay;
            sRX <= DataToWrite(2);
            wait for baud_delay;
            sRX <= DataToWrite(1);
            wait for baud_delay;
            sRX <= DataToWrite(0);
            wait for baud_delay;
            
            sRX <= '1'; -- STOP
            wait for baud_delay;
		
	end procedure UARTWrite;
    
end package body UART_Testbench_Package;