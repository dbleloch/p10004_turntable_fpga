-------------------------------------------------------------
-- Xyratex CONFIDENTIAL 
-- (C) Copyright Xyratex 2012 All rights reserved
-------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.ALL;
use ieee.numeric_std.all;

entity baud_rate_gen is
    generic (
        -- baud_counter        : integer := 868 -- defaults to 115200 with 100MHz sys clock
        baud_counter        : integer := 434 -- defaults to 115200 with 50MHz sys clock
        );
    port (
        Clock                   : in  std_logic;
        Reset                   : in  std_logic;
        UartClockCount          : out std_logic_vector(15 downto 0);
        UartClockEnable         : out std_logic        
    );
end entity baud_rate_gen;

architecture baud_rate_gen_arch of baud_rate_gen is
    
    -- signal baud_counter_s : std_logic_vector(15 downto 0) := Std_logic_vector(To_unsigned(baud_counter,16));
begin

    baud_rate_P: process (Clock,Reset)
        variable baud_counter_v : std_logic_vector(15 downto 0) := (others=>'0');
    begin
        if Reset='1' then
            UartClockEnable <= '0';
            UartClockCount <= Std_logic_vector(To_unsigned(baud_counter,16));
            baud_counter_v := Std_logic_vector(To_unsigned(baud_counter,16));
        elsif rising_edge(Clock) then
            if baud_counter_v = x"0000" then
                UartClockEnable <= '1';
                baud_counter_v := Std_logic_vector(To_unsigned(baud_counter,16));
            else
                UartClockEnable <= '0';
                baud_counter_v := baud_counter_v - 1;
            end if;
        end if;
        
    end process baud_rate_P;

end architecture baud_rate_gen_arch;

