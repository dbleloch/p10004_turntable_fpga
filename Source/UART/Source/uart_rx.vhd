-------------------------------------------------------------
-- Xyratex CONFIDENTIAL 
-- (C) Copyright Xyratex 2012 All rights reserved
-------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.ALL;

entity uart_rx is

	Port(	
        --Misc
        Clock                   : in  std_logic;
        Reset                   : in  std_logic;
        -- Avalon
        AvalonStream_data       : out  std_logic_vector(7 downto 0);
        AvalonStream_valid      : out  std_logic;

        -- Uart control
        UartClockCount          : in  std_logic_vector(15 downto 0);

        -- Uart Interface
        rx                      : in std_logic
	);
end entity uart_rx;

architecture uart_rx_arch of uart_rx is    

    signal Rx_hardened : std_logic;
    
begin

    Rx_metastability_hardening_P: process (Clock,Reset)
        variable meta : std_logic;
    begin
        if Reset='1' then
            Rx_hardened <= '1';
            meta := '1';
        elsif rising_edge(Clock) then
            Rx_hardened <= meta;
            meta := Rx;
        end if;
        
    end process Rx_metastability_hardening_P;
    

    process(Clock,Reset)
        variable shift : std_logic_vector(7 downto 0); 
        variable bitstogo : std_logic_vector(3 downto 0);
        variable rx_r : std_logic;
        variable downcounter : std_logic_vector(15 downto 0);
    begin
        if (Reset='1') then
            shift       := (others=>'0');
            downcounter := (others=>'0');
            bitstogo    := (others=>'0');
            
        elsif rising_edge(Clock) then
            AvalonStream_valid   <= '0'; -- default assignment

            -------------------------------------------------------
            -- Wait for RX Falling when there no bits to go.
            -------------------------------------------------------
            if Rx_hardened='0' and rx_r='1' and bitstogo=x"0" then 
                bitstogo := x"A";
                downcounter := '0' & UartClockCount(15 downto 1);   -- Count down half a bit period
                shift := (others=>'-');
            end if;
            
            if bitstogo/=x"0"  then
            
                if downcounter = x"00" then
                    downcounter := UartClockCount;
                    
                    shift := Rx_hardened & shift(7 downto 1);
                    bitstogo := bitstogo - '1';
                    if bitstogo =x"1" then
                        AvalonStream_valid <= '1';
                    end if;
                else 
                    downcounter := downcounter - '1';
                end if;
            end if;        
            AvalonStream_data <= shift;
            
            rx_r := Rx_hardened;
            
        end if;
    end process;




end architecture uart_rx_arch;

