#!/bin/bash
set -e  # quit if any command fails

echo "Start from fresh & delete all old libraries"
rm -f *.o
rm -f *.cf
ghdl --clean


echo "Compiling C_Package"
ghdl -a --ieee=synopsys --work=C ./Common/C_Package/ctype_h.vhd 
ghdl -a --ieee=synopsys --work=C ./Common/C_Package/debugio_h.vhd
ghdl -a --ieee=synopsys --work=C ./Common/C_Package/endian_h.vhd
ghdl -a --ieee=synopsys --work=C ./Common/C_Package/strings_h.vhd
ghdl -a --ieee=synopsys --work=C ./Common/C_Package/regexp_h.vhd 
ghdl -a --ieee=synopsys --work=C ./Common/C_Package/stdlib_h.vhd
ghdl -a --ieee=synopsys --work=C ./Common/C_Package/stdio_h.vhd

echo "Compiling Random & math"
ghdl -a --ieee=synopsys --work=work ./Common/RandomNumberGenerator/Source/math_lib.vhd
ghdl -a --ieee=synopsys --work=work ./Common/RandomNumberGenerator/Source/rng_lib.vhd

#echo "Compiling Altera MF Package"
#ghdl -a --ieee=synopsys -fexplicit --work=altera_mf /opt/altera11.1/modelsim_ae/altera/vhdl/src/altera_mf/altera_mf_components.vhd 
#ghdl -a --ieee=synopsys -fexplicit --work=altera_mf /opt/altera11.1/modelsim_ae/altera/vhdl/src/altera_mf/altera_mf.vhd 

echo "Compiling UnitTest"
ghdl -a --ieee=synopsys            --work=work ./Common/unittest/UnitTestPackage.vhd 
ghdl -a --ieee=synopsys            --work=work ./Common/unittest/unittest_tb.vhd 


echo "Compiling UART"
ghdl -a --ieee=synopsys -fexplicit --work=work ./uart/uart_tx.vhd
ghdl -a --ieee=synopsys -fexplicit --work=work ./uart/uart_rx.vhd
ghdl -a --ieee=synopsys -fexplicit --work=work ./uart/uart_tx_tb.vhd
ghdl -a --ieee=synopsys -fexplicit --work=work ./uart/uart_tb.vhd


echo "Elaborating Testbenches"
ghdl -e --ieee=synopsys -fexplicit uart_tx_tb 
ghdl -e --ieee=synopsys -fexplicit uart_tb 

echo "Simulate"

#ghdl -r uart_tx_tb   --vcd=fred.vcd    --ieee-asserts=disable --stop-time=10us 
ghdl -r uart_tb   --vcd=fred.vcd    --ieee-asserts=disable --stop-time=100us 



