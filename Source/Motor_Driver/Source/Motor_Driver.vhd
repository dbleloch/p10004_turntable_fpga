LIBRARY ieee ;
USE ieee.std_logic_1164.all ;
USE ieee.numeric_std.all ;
-- USE ieee.std_logic_arith.all ;
-- USE ieee.std_logic_unsigned.all ;


ENTITY Motor_Driver IS
    GENERIC ( n : INTEGER := 14 ) ;
    PORT (
         -- clk             : IN STD_LOGIC ;
         motor_clk            : IN STD_LOGIC ;
         reset                : IN STD_LOGIC ;
         direction            : IN STD_LOGIC ;
         enable               : IN STD_LOGIC ;
         counter_reset        : IN STD_LOGIC ;
         motor_position       : OUT STD_LOGIC_VECTOR (15 DOWNTO 0) ;
         motor_outputs        : OUT STD_LOGIC_VECTOR(3 downto 0) 
         ) ;
END Motor_Driver ;

ARCHITECTURE rtl OF Motor_Driver IS

    signal motor_state : STD_LOGIC_VECTOR(3 downto 0) := "0001" ;
    signal counter : INTEGER range 0 to ((2**16) - 1);
                
BEGIN

    -- Light 7-seg display
    process_drive_motor : PROCESS ( motor_clk, motor_state, reset )
    BEGIN
        IF reset = '1'  then
            motor_state <= motor_state;
        ELSIF motor_clk'EVENT AND motor_clk = '1' THEN
            -- check for zero counter
            motor_state <= motor_state ; 
            IF enable = '1' THEN 
                IF direction = '1' THEN
                    -- IF motor_state = "0001" THEN motor_state <= "0011" ; END IF ;
                    -- IF motor_state = "0011" THEN motor_state <= "0010" ; END IF ;
                    -- IF motor_state = "0010" THEN motor_state <= "0110" ; END IF ;
                    -- IF motor_state = "0110" THEN motor_state <= "0100" ; END IF ;
                    -- IF motor_state = "0100" THEN motor_state <= "1100" ; END IF ;
                    -- IF motor_state = "1100" THEN motor_state <= "1000" ; END IF ;
                    -- IF motor_state = "1000" THEN motor_state <= "1001" ; END IF ;
                    -- IF motor_state = "1001" THEN motor_state <= "0001" ; END IF ;
                    CASE motor_state IS
                        WHEN "0001" => motor_state <= "0011";
                        WHEN "0011" => motor_state <= "0010";
                        WHEN "0010" => motor_state <= "0110";
                        WHEN "0110" => motor_state <= "0100";
                        WHEN "0100" => motor_state <= "1100";
                        WHEN "1100" => motor_state <= "1000";
                        WHEN "1000" => motor_state <= "1001";
                        WHEN "1001" => motor_state <= "0001";
                        WHEN OTHERS => motor_state <= "0001";
                        END CASE ;
                ELSE
                    -- IF motor_state = "0001" THEN motor_state <= "1001" ; END IF ;
                    -- IF motor_state = "0011" THEN motor_state <= "0001" ; END IF ;
                    -- IF motor_state = "0010" THEN motor_state <= "0011" ; END IF ;
                    -- IF motor_state = "0110" THEN motor_state <= "0010" ; END IF ;
                    -- IF motor_state = "0100" THEN motor_state <= "0110" ; END IF ;
                    -- IF motor_state = "1100" THEN motor_state <= "0100" ; END IF ;
                    -- IF motor_state = "1000" THEN motor_state <= "1100" ; END IF ;
                    -- IF motor_state = "1001" THEN motor_state <= "1000" ; END IF ;
                    CASE motor_state IS
                        WHEN "0001" => motor_state <= "1001";
                        WHEN "0011" => motor_state <= "0001";
                        WHEN "0010" => motor_state <= "0011";
                        WHEN "0110" => motor_state <= "0010";
                        WHEN "0100" => motor_state <= "0110";
                        WHEN "1100" => motor_state <= "0100";
                        WHEN "1000" => motor_state <= "1100";
                        WHEN "1001" => motor_state <= "1000";
                        WHEN OTHERS => motor_state <= "1000";
                        END CASE ;
                END IF ;
            END IF ;
        END IF ;
    END PROCESS ;
    
    process_motor_position : PROCESS ( motor_clk, counter_reset, reset )
    BEGIN
        IF reset = '1'  or counter_reset = '1' then
            counter <= 0;
        ELSIF motor_clk'EVENT AND motor_clk = '1' THEN
            -- check for zero counter
            counter <= counter + 1 ; 
        END IF ;
    END PROCESS ;
        
    motor_outputs <= motor_state ;
        
    motor_position <= std_logic_vector(to_unsigned(counter, motor_position'length)) ;
    
END rtl;

