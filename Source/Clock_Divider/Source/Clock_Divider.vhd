-------------------------------------------------------------
-- Xyratex CONFIDENTIAL
-- (C) Copyright Xyratex 2007 All rights reserved
-------------------------------------------------------------
-- StarTeam updated fields
-- Project:     $Project:$
-- Filename:    $Workfile:$
-- Revision:    $Revision:$
-- TimeStamp:   $Date:$
-- Author:      $Author:$
-------------------------------------------------------------
-- Description: Timer for the LEDs
-------------------------------------------------------------
-- Log: $Log:
-- Log: $
-------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity Clock_Divider is
    generic (
        Counter_Max_Value   : in std_logic_vector(27 downto 0) := X"17D7840" -- 50MHz -> 1Hz for LED
                                                               -- X"000E100" -- 50MHz -> ?Hz for stepper motor
                                                               -- X"0000010" -- use for testbenching
        );
	port (
		Clock_In            : in  std_logic;
		Reset               : in  std_logic;
		Clock_Out           : out std_logic
        );
end Clock_Divider;

architecture rtl of Clock_Divider is
    ------------------------------------------------------------------
    --                     Main Signals
    ------------------------------------------------------------------
    signal Counter                       : std_logic_vector(Counter_Max_Value'length downto 0);
    signal Div_Clock                     : std_logic;  -- divided down clock
    ------------------------------------------------------------------
begin
    -------------------------------------------------------------------
    -- Main Counter, Divides the 100MHZ Clock into 1HZ Clock
    -------------------------------------------------------------------
    process(Clock_In,Reset)
	begin
			if Reset = '1'  then
				Counter	<= (others=>'0');
				Div_Clock <= '0';	
			elsif rising_edge(Clock_In) then
                if (Counter = Counter_Max_Value) then
                    Counter	<= (others => '0');
                    Div_Clock <= NOT Div_Clock;
                else
                    Counter<=Counter+'1';                               
                end if;       
            end if;
    end process;  
    -- Update the Output
    Clock_Out <= Div_Clock; 
    -------------------------------------------------------------------
end rtl;




