LIBRARY ieee ;
USE ieee.std_logic_1164.all ;
-- USE ieee.numeric_std.all ;

----------------------
-- Top-level module --
----------------------

ENTITY Turntable_Top_Level IS

    PORT
        (
        in_clock                    : IN STD_LOGIC ;
        in_resetn                   : IN STD_LOGIC ;
        top_level_motor_outputs     : OUT STD_LOGIC_VECTOR (3 DOWNTO 0) ;
        top_level_led_1             : OUT STD_LOGIC ;
		  top_level_led_2             : OUT STD_LOGIC ;
		  top_level_led_3             : OUT STD_LOGIC ;
		  
        top_level_Rx_in             : IN STD_LOGIC ;
        top_level_Tx_out            : OUT STD_LOGIC ; 
		  
        top_level_button_input      : IN STD_LOGIC ;
        top_level_opto_input        : IN STD_LOGIC 		  
        ) ;
    END Turntable_Top_Level ;

    
ARCHITECTURE rtl OF Turntable_Top_Level IS

    component avalon_sys is
        port (
            clk_clk                             : in  std_logic                     := 'X'; -- clk
            reset_reset_n                       : in  std_logic                     := 'X'; -- reset_n
            uart_master_0_conduit_end_TX        : out std_logic;                            -- TX
            uart_master_0_conduit_end_RX        : in  std_logic                     := 'X'; -- RX
            pio_0_external_connection_export    : out std_logic_vector(31 downto 0)         -- export
            );
        end component avalon_sys;

    component Debounce is
        generic(
            N : integer
            ) ;
        port(
            clk, nreset : in std_logic;
            button_in   : in std_logic;
            DB_out      : buffer std_logic
            );
        end component Debounce;

    component Motor_Driver
        port ( 
            -- Clock inputs and Reset
            motor_clk            : IN STD_LOGIC ;
            direction            : IN STD_LOGIC ;
            enable               : IN STD_LOGIC ;
            reset                : IN STD_LOGIC ;
            counter_reset        : IN STD_LOGIC ;
            motor_position       : OUT STD_LOGIC_VECTOR (15 DOWNTO 0) ;
            motor_outputs        : OUT STD_LOGIC_VECTOR (3 DOWNTO 0) 
            );
		end component Motor_Driver;
   
    component Clock_Divider IS
        GENERIC (
            Counter_Max_Value   : in std_logic_vector(27 downto 0)
            );
        PORT (
            Clock_In         : IN STD_LOGIC ;
            Reset            : IN STD_LOGIC  := '1';
            Clock_Out        : OUT STD_LOGIC
            );
        END component Clock_Divider;
         
    signal top_level_motor_clock         : std_logic := '0';
    signal top_level_direction           : std_logic := '1';
    signal top_level_enable              : std_logic := '1';
    signal top_level_OK                  : std_logic := '1';
     --==================================
    signal in_reset                      : std_logic;
     --==================================
    signal top_level_button              : std_logic;
    signal top_level_opto                : std_logic;
	 signal top_level_slow_flash          : std_logic;
    
    -- signal top_level_current_location    : std_logic_vector(31 downto 0) := "00001111";
    -- signal top_level_destination         : std_logic_vector(31 downto 0) := "11110000";

    -- signal top_level_AvalonChipSelect    : std_logic;
    -- signal top_level_AvalonAddress       : std_logic_vector(31 downto 0);
    -- signal top_level_AvalonRead          : std_logic;
    -- signal top_level_AvalonReadData      : std_logic_vector(31 downto 0);
    -- signal top_level_AvalonWrite         : std_logic;
    -- signal top_level_AvalonWriteData     : std_logic_vector(31 downto 0);
    -- signal top_level_AvalonWaitRequest   : std_logic;

    signal PIO_signals                   : std_logic_vector(31 downto 0);

BEGIN

    in_reset <= '0' ;
    top_level_led_1 <= PIO_signals(0);
	 top_level_led_2 <= top_level_slow_flash;
	 top_level_led_3 <= not top_level_slow_flash;
    top_level_direction <= PIO_signals(1);
	 top_level_enable <= PIO_signals(2);
	 
    u_avalon : component avalon_sys
        port map (
            clk_clk                             => in_clock,                --                              clk.clk
            reset_reset_n                       => not in_reset,            --                            reset.reset_n
            uart_master_0_conduit_end_TX        => top_level_Tx_out,        -- avalon_master_uart_0_conduit_end.TX
            uart_master_0_conduit_end_RX        => top_level_Rx_in,         --                                 .RX
            pio_0_external_connection_export    => pio_signals              --        pio_0_external_connection.export
            );
  
    u_button_debouncer : component Debounce
        generic map (
            N => 23
            ) 
        port map (
            clk         => in_clock,
            nreset      => not in_reset,
            button_in   => top_level_button_input,
            DB_out      => top_level_button
            );

    u_slotted_opto_debouncer : component Debounce
        generic map (
            N => 23
            ) 
        port map (
            clk         => in_clock,
            nreset      => not in_reset,
            button_in   => top_level_opto_input,
            DB_out      => top_level_opto
            );

    u_motor_clock_gen : Clock_Divider
    generic map (
        Counter_Max_Value                => X"000E100"
        )
    port map ( 
        -- Clock inputs and Reset
        Clock_In                       => in_clock,
        Reset                          => in_reset,
        Clock_Out                      => top_level_motor_clock
        );

    u_motor_driver : motor_driver
    port map ( 
        -- Clock inputs and Reset
        motor_clk                      => top_level_motor_clock,
        reset                          => in_reset,
        enable                         => top_level_enable,
        direction                      => top_level_direction,
        counter_reset                  => '0',
        motor_position                 => open,
        motor_outputs                  => top_level_motor_outputs      
        );
          
    u_heartbeat_led_clock_divider : Clock_Divider
    generic map (
        Counter_Max_Value                => X"17D7840"
        )
    port map ( 
        -- Clock inputs and Reset
        Clock_In                      => in_clock,
        Reset                         => '0',
        Clock_Out                     => top_level_slow_flash
        );
         
END rtl;

